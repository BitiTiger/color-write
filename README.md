# Color Write

Color Write is a Python module that writes colored text to the terminal.

## Usage

```python
import colorWrite as cw

cw.write("My Text", cw.foregroundColor.COLOR, cw.backgroundColor.COLOR)
```

## Color Options

**The following colors are avalible for the foreground:**

* WHITE
* BLACK
* LIGHT_GREY
* DARK_GREY
* RED
* LIGHT_RED
* ORANGE
* YELLOW
* GREEN
* LIGHT_GREEN
* LIGHT_TEAL
* TEAL
* LIGHT_BLUE
* BLUE
* PURPLE
* MAGENTA

**The following colors are avalible for the background:**

* BLACK
* LIGHT_GREY
* DARK_GREY
* RED
* LIGHT_RED
* ORANGE
* YELLOW
* GREEN
* LIGHT_GREEN
* LIGHT_TEAL
* TEAL
* LIGHT_BLUE
* BLUE
* PURPLE
* MAGENTA

## Testing

There is a test program provided in the `src` folder for texting the library. An example of the test program is provided below.

![screenshot](Screenshot.png)