# This code requires the colorWrite module in order to test its behavior.
import colorWrite as cw

# The test0 function is the first test performed on the colorWrite module. The
# function uses a counter to list all the forground colors in a 4x4 grid with
# each color being justified to the right of the column. The function then
# provides a decription of what the user should see and then prompts if the
# output matches the description. The result of the prompt is then returned.
# BEHAVIOR:
#    make a 4x4 grid of foreground colors and ask if they are correct
# RETURN:
#    true if output matches description
#    false if output does not match description
def test0():
    counter = 0
    for fg in cw.foregroundColor:
        if counter % 4 == 3:
            cw.writeline(fg.name,fg)
        else:
            cw.write("%11s\t" % (fg.name),fg)
        counter += 1
    print("You should see a 4x4 grid of colored text with every color properly labled.")
    return prompt()

# The test1 function is the second test performed on the colorWrite module. The
# function iterates over every foreground and background color and prints a
# grid to the terminal. The grid uses the foreground colors for the rows and
# background colors for the columns. The function then provides a descrition of
# what the user should see and then prompts if the output matches the
# descrition. The result of the prompt is then returned.
# BEHAVIOR:
#    make grid matching every foreground color to every background color and
#    ask if they match the descrition
# RETURN:
#    true if output matches descrition
#    false if output does not match descrition
def test1():
    for fg in cw.foregroundColor:
        for bg in cw.backgroundColor:
            cw.write("#",fg,bg)
        print()
    print("You should see a grid of every foreground color on top of every background color.")
    return prompt()

# The prompt function asks the user if the output of a test matches the test's
# descrition. The answer is then converted to lowercase and then tested if the
# user entered yes or no to the question. If the user said yes, the function
# returns true. If the user said no, the function returns false. If the input
# doesn't say yes or no, the function calls itself again.
# BEHAVIOR:
#    ask if the output of a test matched its descrition and return a boolean
#    stating if it passed
# RETURN:
#    true if user said `yes`
#    false if user said `no`
def prompt():
    ans = input("Did the output match the description (yes/no)? ").lower()
    if ans == "yes":
        return True
    elif ans == "no":
        return False
    else:
        return prompt()

# The check function runs every test and checks if it passed. If any fails,
# it sets the `passed` flag to false. Lastly, the function returns the result
# of the flag.
# BEHAVIOR:
#    run every test and return false if any of the tests fail
# RETURN:
#    true if all the tests pass
#    false if any tests fail
def check():
    passed = True
    if passed and not test0(): passed = False
    if passed and not test1(): passed = False
    return passed

# The main function runs the check function and tests the result. If the check
# method returns true, it tells the user that all the tests passed. If the
# check function returns false, it tells the user that a test failed.
# BEHAVIOR:
#    run the check function and write the result
def main():
    if check():
        print("All tests passed.")
    else:
        print("One or more tests failed")

# call the main function and begin the program
main()
